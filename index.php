<?php
$screen = $_REQUEST['screen'];
if (!$screen) {
    $dirs = array_filter(glob('*'), 'is_dir');
} else {
    $fileList = glob($screen . '/*');
}
?>
<html>
    <head>
        <title>Ariane 6 Info Kiosk</title>
        <script type="text/javascript" src="jquery-3.4.1.min.js"></script>
        <script>
            $(document).ready(function () {
<?php
if ($fileList) {
    echo 'var pics = new Array();';
    $i = 0;
    foreach ($fileList as $file) {
        echo 'pics[' . $i . '] = "' . $file . '";';
        $i++;
    }
//    echo 'console.log(pics);';
}
?>

                function loopPics(pics) {
                    displayTime = 3000;
                    try {
                        $(function () {
                            var dataArray = pics;
                            var startTime = performance.now();
                            window.setInterval(function () {
                                var calcTime = performance.now();
                                var runTime = Math.round(calcTime - startTime);
                                if (runTime >= displayTime) {
                                    startTime = performance.now();
                                }
                                percentage = Math.round(100 - ((runTime * 100) / displayTime));
                                $(".progessBar").css("width", percentage + "%");
                                console.log(percentage);
                            }, 1);
                            $(".kioskCanvas").css("background-image", "url(" + dataArray[0] + ")");
                            
                            var thisId = 1;

                            window.setInterval(function () {
                                var startTime = performance.now();
                                window.setInterval(function () {
                                    var calcTime = performance.now();
                                    var runTime = Math.round(calcTime - startTime);
                                    if (runTime >= displayTime) {
                                        startTime = performance.now();
                                    }
                                    percentage = Math.round(100 - ((runTime * 100) / displayTime));
                                    $(".progessBar").css("width", percentage + "%");
                                    console.log(percentage);
                                }, 1);


                                $(".kioskCanvas").css("background-image", "url(" + dataArray[thisId] + ")");
                                thisId = thisId + 1;
                                if (thisId == dataArray.length + 1) {
//                                    thisId = 0; //repeat from start
                                    location.reload();
                                }
                            }, displayTime);
                        });
                    } catch (e) {
                        console.log("no pics");
                        location.reload();
                    }
                }
                try {
                    loopPics(pics);
                } catch (e) {
                }

                $("#back").on('click', function () {
                    window.location = "index.php";
                });
            });
        </script>
        <style>
            html, body{
                padding: 0;
                margin: 0;
                font-family: verdana;
            }
            .kioskCanvas{
                background-color: #0069C8;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                width: 100%;
                height: 100%;
            }
            #back{
                position: absolute;
                width: 100px;
                height: 100px;
                cursor: pointer;
                top: 0;
                right: 0;
            }
            .indexDiv{
                width: 100%;
                text-align: center;
                margin-top: 10%;

            }
            .progressWrapper{
                position: absolute;
                bottom: 0px;
                left: 0px;
                width:100%;
                height:10px;
            }
            .progessBar{
                position: absolute;
                bottom: 0px;
                left: 0px;
                height:1px;
                background-color: white;
                width:100%;
                z-index: 5;
            }
        </style>
    </head>

    <body>
        <div id="back"></div>
        <?php
        if (!$screen) {
            echo '<div class="indexDiv"><ol>';
            foreach ($dirs as $dir) {
                echo '<h2><a href="index.php?screen=' . $dir . '">' . $dir . '</a></h2>';
            }
            echo '</ol></index>';
        } else {
            echo '<div class="kioskCanvas"><div class="progressWrapper"><div class="progessBar"></div></div></div>';
        }
        ?>

    </body>
</html>
